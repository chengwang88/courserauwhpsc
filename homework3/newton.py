# $UWHPSC/codes/homework3/test_code.py 
# To include in newton.py

def solve(fvals_sqrt, x0, debug=False):
    maxiter = 20
    tol = 1.e-14
    if debug:
        print "Initial guess: x = %22.15e" % x0
    for i in range(maxiter):
        f, fp = fvals_sqrt(x0)
        if abs(f) < tol:
            break
        delta = f/fp
        x0 = x0 - delta
        if debug:
            print "After %s interations, x = %22.15e" %(i+1, x0)
    return x0, i


def fvals_sqrt(x):
    """
    Return f(x) and f'(x) for applying Newton to find a square root.
    """
    f = x**2 - 4.
    fp = 2.*x
    return f, fp

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x
