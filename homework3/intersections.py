import numpy as np
from newton import solve

def g1(x):
    return x * np.cos(np.pi * x)

def g2(x):
    return 1 - 0.6 * x**2

def g12(x):
    g = g1(x) - g2(x)
    gp = np.cos(np.pi * x) - x * np.sin(np.pi * x) * np.pi + 1.2 * x
    return g, gp


def plotinter(x,y):
    import matplotlib.pyplot as plt 
    xi = np.linspace(-5,5,1000)
    g1 = xi * np.cos(np.pi * xi)
    g2 = 1 - 0.6 * xi**2
    plt.figure(1)
    plt.plot(xi,g1,'b-',xi,g2,'r-')
    plt.plot(x,y,'ko')
    plt.xlim(-3,2)
    plt.ylim(-5,5)
    plt.show()
    plt.savefig("intersections.png")

def test1(debug_solve=False):
    """
    """
    x = []
    y = []
    for x0 in [-2.2, -1.6, -0.8, 1.5]:
        print " "  # blank line
        res,iters = solve(g12, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (res,iters)
        x.append(res)
    for i in x:
        y.append(g1(i))
    plotinter(x,y)

if __name__ == "__main__":
    print "Running test..."
    test1(True)
